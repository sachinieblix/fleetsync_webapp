var express = require('express');
var app = express();
app.use(express.static(__dirname + '/public'));
var bodyParser = require('body-parser');
var multer = require('multer'); 
var mongoose = require('mongoose');
var funct = require('functions/functions');
var basic = require('functions/basic');
var logins = require('functions/login');
var passport = require('passport');
var Login = require('models/login');
var Taxilist = require('models/taxilist');
var Userorder = require('models/userorder');
var LocalStrategy = require('passport-local').Strategy;

app.use(express.cookieParser()); 

app.use(express.methodOverride());
app.use(express.session({ secret: 'securedsession' }));
app.use(passport.initialize());  // Add passport initialization
app.use(passport.session());
app.use(bodyParser.json());

app.use(express.favicon());
app.use(bodyParser.urlencoded({extended:true})); // for parsing application/json
app.use(multer());// for parsing multipart/form-data

var connectionstring =process.env.OPENSHIFT_MONGODB_DB_URL||'mongodb://localhost/fleetsyncdb';
mongoose.connect(connectionstring);
var ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var port = process.env.OPENSHIFT_NODEJS_PORT || 3000;


//test

var logg = new Login({
username:"sachini",
password:"sachini"

});
 
logg.save(function (err, data) {
if (err) console.log(err);
//else console.log('Saved ', data );
});
var order = new Userorder({

OderID: "OP002", To: "Kaduwela",From:"Battaramulla",ExactGPSpickup:{lat:9,lon:89},ExactGPSDropoff:{lat:9,lon:89},assigntaxi:"T001"
});

order.save(function (err, data) {
if (err) console.log(err);
//else console.log('Saved ', data );
});
//
 
passport.use(new LocalStrategy(
  function(username, password,done) {

    Login.findOne({ $and:[ {'username':username}, {'password':password}]}, function(err, user) {

      if (err) {
        return done(err);
      }
 
      if (!user) {
        return done(null, false,{ message: 'Invalid user' });
      }
 
        return done(null, user);


    }); 
        
  }
));

// Serialized and deserialized methods when got from session
passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

//Define a middleware function to be used for every secured routes
var auth = function(req, res, next){
  if (!req.isAuthenticated()) 
    res.send("401");
  else
    next();
};



app.post('/route',function(req,res){
        
        var order=req.body;
        
        funct.routing(order,function (found) {
            
            res.json(found);
        });     
    });

app.get('/orders',function(req,res){
        
      
        
        basic.vieworder(function (found) {
            
            res.json(found);
        });     
    });

app.post('/acceptReq',function(req,res){
        
        var accptd=req.body;
        
        funct.custaccptd(accptd,function (found) {
            
            res.json(found);
        });     
    });

app.get('/showtaxi/:rgtrno',function(req,res){
            var taxid=req.params['rgtrno'];
            console.log(taxid);
            basic.rgstrtaxi(taxid,function (found) {
            
                res.json(found);
            

        }); 
        

});
app.get('/getCompany',function(req,res){
           
       
            basic.getCompany(function (found) {
               
                res.json(found);
            

        }); 
        

});
app.post('/getrequest',function(req,res){
        
      
        var vehicleid=req.body.id;
        basic.getrequest(vehicleid,function (found) {
            
            res.json(found);
        });     
    
    });
app.get('/showdriver/:rgtrno',function(req,res){
            var driverid=req.params['rgtrno'];
            console.log(driverid);
            basic.rgstrdriver(driverid,function (found) {
            
                res.json(found);
            

        }); 
        

});

app.post('/addtaxis',function(req,res){
        var newtaxi=req.body;
        basic.rgstraddtaxi(newtaxi,function (found) {
            
                res.json(found);
            

        }); 
                
});
app.post('/adddrivers',function(req,res){
        var newdriver=req.body;
        console.log(newdriver);
        basic.rgstradddriver(newdriver,function (found) {
            
                res.json(found);
                
            

        }); 
                
});
app.delete('/deletetaxis/:id',function(req,res){
        var id=req.params['id'];
        
        basic.removetaxi(id,function (found) {
            
                res.json(found);
            

        }); 
                
});

app.delete('/deletedriver/:id',function(req,res){
        var id=req.params['id'];
        
        basic.remvdriver(id,function (found) {
            
                res.json(found);
            

        }); 
                
});

app.post('/authent', passport.authenticate('local'), function(req, res) {
 
  res.json(req.user);
  
 
});
app.get('/loggedin', function(req, res) {
  res.send(req.isAuthenticated() ? req.user : '0');
});
// route to log out
app.post('/logout', function(req, res){
  req.logOut();
  res.send(200);
});


app.listen(port,ip);
